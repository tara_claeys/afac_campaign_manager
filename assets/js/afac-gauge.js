jQuery(function($){

	newVal=$('.progress--gauge').data('progress');
	$('.progress--gauge .semi-circle--mask').attr({
	    style: '-webkit-transform: rotate(' + newVal + 'deg);' +
	    '-moz-transform: rotate(' + newVal + 'deg);' +
	    'transform: rotate(' + newVal + 'deg);'
	});
});
