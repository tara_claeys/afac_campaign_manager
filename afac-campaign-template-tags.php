<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! function_exists( 'afac_featured_campaigns' ) ) :

	function afac_get_featured_campaigns(){

		$campaigns = new AFAC_Campaign_Query( array(
			'meta_key'		=> '_afac_campaign_featured',
			'meta_value'	=> 'on',
			'meta_compare'	=> '='
		) );

		if( ! $campaigns ) return false;

		return $campaigns;
	}

endif;

if( ! function_exists('afac_dropdown_campaigns') ) :

	/**
	 * Retrieve or display list of campaigns as a dropdown (select list).
	 *
	 * @since 1.0.0
	 *
	 * @param array|string $args {
	 *     Optional. Array or string of arguments to generate a campaigns drop-down element.
	 *
	 *     @type int|string   $selected              Value of the option that should be selected. Default 0.
	 *     @type bool|int     $echo                  Whether to echo or return the generated markup. Accepts 0, 1,
	 *                                               or their bool equivalents. Default 1.
	 *     @type string       $name                  Value for the 'name' attribute of the select element.
	 *                                               Default 'campaign_id'.
	 *     @type string       $id                    Value for the 'id' attribute of the select element.
	 *     @type string       $class                 Value for the 'class' attribute of the select element. Default: none.
	 *                                               Defaults to the value of `$name`.
	 *     @type string       $show_option_none      Text to display for showing no campaigns. Default empty (does not display).
	 *     @type string       $show_option_no_change Text to display for "no change" option. Default empty (does not display).
	 *     @type string       $option_none_value     Value to use when no campaign is selected. Default empty.
	 *     @type string       $value_field           Post field used to populate the 'value' attribute of the option
	 *                                               elements. Accepts any valid post field. Default 'ID'.
	 * }
	 * @return string HTML content, if not displaying.
	 */
	function afac_dropdown_campaigns( $args = '' ) {
		$defaults = array(
			'selected' => 0, 'echo' => 1,
			'name' => 'campaign_id', 'id' => '',
			'class' => '',
			'show_option_none' => '', 'show_option_no_change' => '',
			'option_none_value' => '',
			'value_field' => 'ID',
		);

		$r = wp_parse_args( $args, $defaults );

		$campaigns = afac_get_campaigns( $r );

		$output = '';
		// Back-compat with old system where both id and name were based on $name argument
		if ( empty( $r['id'] ) ) {
			$r['id'] = $r['name'];
		}

		if ( ! empty( $campaigns ) ) {
			$class = '';
			if ( ! empty( $r['class'] ) ) {
				$class = " class='" . esc_attr( $r['class'] ) . "'";
			}

			$output = "<select name='" . esc_attr( $r['name'] ) . "'" . $class . " id='" . esc_attr( $r['id'] ) . "'>\n";
			if ( $r['show_option_no_change'] ) {
				$output .= "\t<option value=\"-1\">" . $r['show_option_no_change'] . "</option>\n";
			}
			if ( $r['show_option_none'] ) {
				$output .= "\t<option value=\"" . esc_attr( $r['option_none_value'] ) . '">' . $r['show_option_none'] . "</option>\n";
			}
			foreach( $campaigns as $campaign ) :
				$output .= sprintf( '<option value="%s" %s >%s</option>', esc_attr( $campaign->{$r['value_field']} ), selected( $r['selected'], $campaign->{$r['value_field']}, false ), esc_html( $campaign->post_title ) );
			endforeach;

			$output .= "</select>\n";
		}

		/**
		 * Filter the HTML output of a list of campaigns as a drop down.
		 *
		 * @since 2.1.0
		 * @since 4.4.0 `$r` and `$campaigns` added as arguments.
		 *
		 * @param string $output HTML output for drop down list of campaigns.
		 * @param array  $r      The parsed arguments array.
		 * @param array  $campaigns  List of WP_Post objects returned by `afac_get_campaigns()`
		 */
		$html = apply_filters( 'afac_dropdown_campaigns', $output, $r, $campaigns );

		if ( $r['echo'] ) {
			echo $html;
		}
		return $html;
	}

endif;

if( ! function_exists( 'afac_get_campaigns' ) ) :

	/**
	 * Retrieve a list of campaigns.
	 *
	 * @global wpdb $wpdb WordPress database abstraction object.
	 *
	 * @since 1.0.0
	 *
	 * @param array|string $args {
	 *     Optional. Array or string of arguments to retrieve campaigns.
	 *
	 *     @type string       $sort_order   How to sort retrieved campaigns. Accepts 'ASC', 'DESC'. Default 'ASC'.
	 *     @type string       $sort_column  What columns to sort campaigns by, comma-separated. Accepts 'post_author',
	 *                                      'post_date', 'post_title', 'post_name', 'post_modified', 'menu_order',
	 *                                      'post_modified_gmt', 'ID', 'rand', 'comment_count'.
	 *                                      'post_' can be omitted for any values that start with it.
	 *                                      Default 'post_title'.
	 *     @type array        $exclude      Array of campaign IDs to exclude. Default empty array.
	 *     @type array        $include      Array of campaign IDs to include. Cannot be used with
	 										`$exclude`, `$meta_key`, or `$meta_value`.
	 *                                      Default empty array.
	 *     @type string       $meta_key     Only include campaigns with this meta key. Default empty.
	 *     @type string       $meta_value   Only include campaigns with this meta value. Requires `$meta_key`.
	 *                                      Default empty.
	 *     @type string       $authors      A comma-separated list of author IDs. Default empty.
	 *     @type string|array $exclude_tree Comma-separated string or array of campaign IDs to exclude.
	 *                                      Default empty array.
	 *     @type int          $number       The number of campaigns to return. Default 0, or all campaigns.
	 *     @type int          $offset       The number of campaigns to skip before returning. Requires `$number`.
	 *                                      Default 0.
	 *     @type string       $post_type    The post type to query. Default 'afac-campaign'.
	 *     @type string       $post_status  A comma-separated list of post status types to include.
	 *                                      Default 'publish'.
	 * }
	 * @return array|false List of campaigns matching defaults or `$args`.
	 */
	function afac_get_campaigns( $args = array() ) {
		global $wpdb;

		$defaults = array(
			'sort_order' => 'ASC',
			'sort_column' => 'post_title',
			'exclude' => array(), 'include' => array(),
			'meta_key' => '', 'meta_value' => '',
			'authors' => '', 'exclude_tree' => array(),
			'number' => '', 'offset' => 0,
			'post_type' => AFAC_CM_Campaign::CPT, 'post_status' => 'publish',
		);

		$r = wp_parse_args( $args, $defaults );

		$number = (int) $r['number'];
		$offset = (int) $r['offset'];
		$exclude = $r['exclude'];
		$meta_key = $r['meta_key'];
		$meta_value = $r['meta_value'];
		$post_status = $r['post_status'];

			$hierarchical = false;

		// Make sure we have a valid post status.
		if ( ! is_array( $post_status ) ) {
			$post_status = explode( ',', $post_status );
		}
		if ( array_diff( $post_status, get_post_stati() ) ) {
			return false;
		}

		// $args can be whatever, only use the args defined in defaults to compute the key.
		$key = md5( serialize( wp_array_slice_assoc( $r, array_keys( $defaults ) ) ) );
		$last_changed = wp_cache_get( 'last_changed', 'posts' );
		if ( ! $last_changed ) {
			$last_changed = microtime();
			wp_cache_set( 'last_changed', $last_changed, 'posts' );
		}

		$cache_key = "get_campaigns:$key:$last_changed";
		if ( $cache = wp_cache_get( $cache_key, 'posts' ) ) {
			// Convert to WP_Post instances.
			$campaigns = array_map( 'get_post', $cache );
			/** This filter is documented in wp-includes/post.php */
			$campaigns = apply_filters( 'afac_get_campaigns', $campaigns, $r );
			return $campaigns;
		}

		$inclusions = '';
		if ( ! empty( $r['include'] ) ) {
			$exclude = '';
			$meta_key = '';
			$meta_value = '';
			$hierarchical = false;
			$inccampaigns = wp_parse_id_list( $r['include'] );
			if ( ! empty( $inccampaigns ) ) {
				$inclusions = ' AND ID IN (' . implode( ',', $inccampaigns ) .  ')';
			}
		}

		$exclusions = '';
		if ( ! empty( $exclude ) ) {
			$excampaigns = wp_parse_id_list( $exclude );
			if ( ! empty( $excampaigns ) ) {
				$exclusions = ' AND ID NOT IN (' . implode( ',', $excampaigns ) .  ')';
			}
		}

		$author_query = '';
		if ( ! empty( $r['authors'] ) ) {
			$post_authors = preg_split( '/[\s,]+/', $r['authors'] );

			if ( ! empty( $post_authors ) ) {
				foreach ( $post_authors as $post_author ) {
					//Do we have an author id or an author login?
					if ( 0 == intval($post_author) ) {
						$post_author = get_user_by('login', $post_author);
						if ( empty( $post_author ) ) {
							continue;
						}
						if ( empty( $post_author->ID ) ) {
							continue;
						}
						$post_author = $post_author->ID;
					}

					if ( '' == $author_query ) {
						$author_query = $wpdb->prepare(' post_author = %d ', $post_author);
					} else {
						$author_query .= $wpdb->prepare(' OR post_author = %d ', $post_author);
					}
				}
				if ( '' != $author_query ) {
					$author_query = " AND ($author_query)";
				}
			}
		}

		$join = '';
		$where = "$exclusions $inclusions ";
		if ( '' !== $meta_key || '' !== $meta_value ) {
			$join = " LEFT JOIN $wpdb->postmeta ON ( $wpdb->posts.ID = $wpdb->postmeta.post_id )";

			// meta_key and meta_value might be slashed
			$meta_key = wp_unslash($meta_key);
			$meta_value = wp_unslash($meta_value);
			if ( '' !== $meta_key ) {
				$where .= $wpdb->prepare(" AND $wpdb->postmeta.meta_key = %s", $meta_key);
			}
			if ( '' !== $meta_value ) {
				$where .= $wpdb->prepare(" AND $wpdb->postmeta.meta_value = %s", $meta_value);
			}

		}

		if ( 1 == count( $post_status ) ) {
			$where_post_type = $wpdb->prepare( "post_type = %s AND post_status = %s", $r['post_type'], reset( $post_status ) );
		} else {
			$post_status = implode( "', '", $post_status );
			$where_post_type = $wpdb->prepare( "post_type = %s AND post_status IN ('$post_status')", $r['post_type'] );
		}

		$orderby_array = array();
		$allowed_keys = array( 'author', 'post_author', 'date', 'post_date', 'title', 'post_title', 'name', 'post_name', 'modified',
			'post_modified', 'modified_gmt', 'post_modified_gmt', 'menu_order', 'parent', 'post_parent',
			'ID', 'rand', 'comment_count' );

		foreach ( explode( ',', $r['sort_column'] ) as $orderby ) {
			$orderby = trim( $orderby );
			if ( ! in_array( $orderby, $allowed_keys ) ) {
				continue;
			}

			switch ( $orderby ) {
				case 'menu_order':
					break;
				case 'ID':
					$orderby = "$wpdb->posts.ID";
					break;
				case 'rand':
					$orderby = 'RAND()';
					break;
				case 'comment_count':
					$orderby = "$wpdb->posts.comment_count";
					break;
				default:
					if ( 0 === strpos( $orderby, 'post_' ) ) {
						$orderby = "$wpdb->posts." . $orderby;
					} else {
						$orderby = "$wpdb->posts.post_" . $orderby;
					}
			}

			$orderby_array[] = $orderby;

		}
		$sort_column = ! empty( $orderby_array ) ? implode( ',', $orderby_array ) : "$wpdb->posts.post_title";

		$sort_order = strtoupper( $r['sort_order'] );
		if ( '' !== $sort_order && ! in_array( $sort_order, array( 'ASC', 'DESC' ) ) ) {
			$sort_order = 'ASC';
		}

		$query = "SELECT * FROM $wpdb->posts $join WHERE ($where_post_type) $where ";
		$query .= $author_query;
		$query .= " ORDER BY " . $sort_column . " " . $sort_order ;

		if ( ! empty( $number ) ) {
			$query .= ' LIMIT ' . $offset . ',' . $number;
		}

		$campaigns = $wpdb->get_results($query);

		// Sanitize before caching so it'll only get done once.
		$num_campaigns = count($campaigns);
		for ($i = 0; $i < $num_campaigns; $i++) {
			$campaigns[$i] = sanitize_post($campaigns[$i], 'raw');
		}

		// Update cache.
		update_post_cache( $campaigns );

		if ( ! empty( $r['exclude_tree'] ) ) {
			$exclude = wp_parse_id_list( $r['exclude_tree'] );

			$num_campaigns = count( $campaigns );
			for ( $i = 0; $i < $num_campaigns; $i++ ) {
				if ( in_array( $campaigns[$i]->ID, $exclude ) ) {
					unset( $campaigns[$i] );
				}
			}
		}

		$campaign_structure = array();
		foreach ( $campaigns as $campaign ) {
			$campaign_structure[] = $campaign->ID;
		}

		wp_cache_set( $cache_key, $campaign_structure, 'posts' );

		// Convert to WP_Post instances
		$campaigns = array_map( 'get_post', $campaigns );

		/**
		 * Filter the retrieved list of campaigns.
		 *
		 * @since 2.1.0
		 *
		 * @param array $campaigns List of campaigns to retrieve.
		 * @param array $r     Array of afac_get_campaigns() arguments.
		 */
		return apply_filters( 'get_afac_campaigns', $campaigns, $r );
	}

endif;

if( ! function_exists('campaign_is_live') ):

	function campaign_is_live( $post_id = null ){
		$post_id = ( is_null( $post_id ) ? get_the_ID() : $post_id );
		
		$expires_ts = get_post_meta( $post_id, '_afac_campaign_end_date', true );
		
		if( empty( $expires_ts ) ) return true; // no date - never expires
		
		if( ! isValidTimeStamp_afac( $expires_ts ) ){
			$expires_ts = strtotime( $expires_ts );
		} 
		
		$expires_ts += DAY_IN_SECONDS - 1; // sets expiry to the end of the day 

		$tz = wp_timezone();
		$now = current_datetime();
		$UTC = new DateTimeZone('UTC');
		$expires = new DateTime( '@' . $expires_ts, $UTC );
		$expires->setTimeZone($tz);

		return ( $now <= $expires );

	}
endif;

if( ! function_exists('isValidTimeStamp_afac') ){
	function isValidTimeStamp_afac($timestamp){
	    return ((string) (int) $timestamp === $timestamp) 
		   && ($timestamp <= PHP_INT_MAX)
		   && ($timestamp >= ~PHP_INT_MAX);
	}
}


if( ! function_exists( 'the_campaign_date') ):
	
	function get_campaign_date( $post = null ){

		if( is_null($post)) $post_id = get_the_ID();
		if( is_object($post)) $post_id = ( isset( $post->ID) ? $post->ID : $post->id );
		if( is_array($post)) $post_id = ( isset( $post['ID']) ? $post['ID'] : $post['id'] );
		
		$exp = get_post_meta( $post_id, '_afac_campaign_end_date', true );
		if( empty( $exp) ) return FALSE;
		
		$UTC = new DateTimeZone('UTC');
		$end_date = new DateTime( '@'. $exp, $UTC );

		return $end_date->format('m/d/Y');
	}
	
	function the_campaign_date( $post = null ){
		echo get_campaign_date( $post ); 
	}
endif;
