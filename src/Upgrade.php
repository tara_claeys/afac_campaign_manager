<?php

namespace AFAC_CM;

class Upgrade {

	function __construct(){
	
		$current_db_version = get_option('afac_cm_version', 0.0 );
		
		if( version_compare( $current_db_version, 2.1 ) === -1 ){
			// upgrades required for version 2.1 of Afac Campaign Manager
			self::convert_campaign_expire_dates();
		}
		
	}
	
	private function convert_campaign_expire_dates(){
		global $wpdb;
		
		$query = "SELECT *  FROM `{$wpdb->postmeta}` WHERE `meta_key` LIKE '_afac_campaign_end_date' ORDER BY `post_id` ASC";

		$campaigns = $wpdb->get_results( $query, OBJECT );

		$tz = wp_timezone();
		$UTC = new \DateTimeZone('UTC');
		foreach( $campaigns as $campaign ):

			if( empty( $campaign->meta_value ) ) continue; // can't convert empty expire dates

			$time = new \DateTime( $campaign->meta_value, $UTC );
			$time->setTime( 23, 59, 59 );
			$ts = $time->getTimestamp();
			
			add_post_meta( $campaign->post_id, '_afac_campaign_was_enddate', $campaign->meta_value, true );
			update_post_meta( $campaign->post_id, '_afac_campaign_end_date', $ts );
			
		endforeach;
	}


}