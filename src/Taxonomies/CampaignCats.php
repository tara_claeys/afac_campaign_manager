<?php

namespace AFAC_CM\Taxonomies;

class CampaignCats {

	const TAX = 'afac-campaign-cat';

	public function __construct(){
	
	    // Add new taxonomy, make it hierarchical (like categories)
	    $labels = array(
		   'name'              => _x( 'Categories', 'taxonomy general name', 'textdomain' ),
		   'singular_name'     => _x( 'Category', 'taxonomy singular name', 'textdomain' ),
		   'search_items'      => __( 'Search Categories', 'textdomain' ),
		   'all_items'         => __( 'All Categories', 'textdomain' ),
		   'parent_item'       => __( 'Parent Category', 'textdomain' ),
		   'parent_item_colon' => __( 'Parent Category:', 'textdomain' ),
		   'edit_item'         => __( 'Edit Category', 'textdomain' ),
		   'update_item'       => __( 'Update Category', 'textdomain' ),
		   'add_new_item'      => __( 'Add New Category', 'textdomain' ),
		   'new_item_name'     => __( 'New Category Name', 'textdomain' ),
		   'menu_name'         => __( 'Categories', 'textdomain' ),
	    );
 
	    $args = array(
		   'hierarchical'      => true,
		   'labels'            => $labels,
		   'show_ui'           => true,
		   'show_admin_column' => true,
		   'query_var'         => true,
		   'rewrite'           => array( 'slug' => 'campaign-cat' ),
	    );
 
	    register_taxonomy( self::TAX, \AFAC_CM_Campaign::CPT, $args );
 
		/**
		 * Display a custom taxonomy dropdown in admin
		 * @author Mike Hemberger
		 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
		 */
		add_action('restrict_manage_posts', function(){
			global $typenow;

			$post_type = \AFAC_CM_Campaign::CPT; // change to your post type
			$taxonomy  = self::TAX; // change to your taxonomy
			if ($typenow == $post_type) {
				$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
				$info_taxonomy = get_taxonomy($taxonomy);
				wp_dropdown_categories(array(
					'show_option_all' => sprintf( __( 'Show all %s', 'textdomain' ), $info_taxonomy->label ),
					'taxonomy'        => $taxonomy,
					'name'            => $taxonomy,
					'orderby'         => 'name',
					'selected'        => $selected,
					'show_count'      => true,
					'hide_empty'      => true,
				));
			};
		});

		/**
		 * Filter posts by taxonomy in admin
		 * @author  Mike Hemberger
		 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
		 */
		add_filter('parse_query', function( $query ){
			global $pagenow;

			$post_type = \AFAC_CM_Campaign::CPT; // change to your post type
			$taxonomy  = self::TAX; // change to your taxonomy
			$q_vars    = &$query->query_vars;
			if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {

				$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
				$q_vars[$taxonomy] = $term->slug;

			}
		});
	}


}