<?php

namespace AFAC_CM\Taxonomies;

class CampaignTags {

	const TAX = 'afac-campaign-type';

	public function __construct(){

	    // Add new taxonomy, NOT hierarchical (like tags)
	    $labels = array(
		   'name'                       => _x( 'Campaign Types', 'taxonomy general name', 'textdomain' ),
		   'singular_name'              => _x( 'Campaign Type', 'taxonomy singular name', 'textdomain' ),
		   'search_items'               => __( 'Search Campaign Types', 'textdomain' ),
		   'popular_items'              => __( 'Popular Campaign Types', 'textdomain' ),
		   'all_items'                  => __( 'All Campaign Types', 'textdomain' ),
		   'parent_item'                => null,
		   'parent_item_colon'          => null,
		   'edit_item'                  => __( 'Edit Campaign Type', 'textdomain' ),
		   'update_item'                => __( 'Update Campaign Type', 'textdomain' ),
		   'add_new_item'               => __( 'Add New Campaign Type', 'textdomain' ),
		   'new_item_name'              => __( 'New Campaign Type Name', 'textdomain' ),
		   'separate_items_with_commas' => __( 'Separate campaign types with commas', 'textdomain' ),
		   'add_or_remove_items'        => __( 'Add or remove campaign types', 'textdomain' ),
		   'choose_from_most_used'      => __( 'Choose from the most used campaign types', 'textdomain' ),
		   'not_found'                  => __( 'No campaign types found.', 'textdomain' ),
		   'menu_name'                  => __( 'Campaign Types', 'textdomain' ),
	    );
 
	    $args = array(
		   'hierarchical'          => false,
		   'labels'                => $labels,
		   'show_ui'               => true,
		   'show_admin_column'     => true,
		   'update_count_callback' => '_update_post_term_count',
		   'query_var'             => true,
		   'rewrite'               => array( 'slug' => 'campaign-type' ),
	    );
 
	    register_taxonomy( self::TAX, \AFAC_CM_Campaign::CPT, $args );

		/**
		 * Display a custom taxonomy dropdown in admin
		 * @author Mike Hemberger
		 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
		 */
		add_action('restrict_manage_posts', function(){
			global $typenow;

			$post_type = \AFAC_CM_Campaign::CPT; // change to your post type
			$taxonomy  = self::TAX; // change to your taxonomy
			if ($typenow == $post_type) {
				$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
				$info_taxonomy = get_taxonomy($taxonomy);
				wp_dropdown_categories(array(
					'show_option_all' => sprintf( __( 'Show all %s', 'textdomain' ), $info_taxonomy->label ),
					'taxonomy'        => $taxonomy,
					'name'            => $taxonomy,
					'orderby'         => 'name',
					'selected'        => $selected,
					'show_count'      => true,
					'hide_empty'      => true,
				));
			};
		});

		/**
		 * Filter posts by taxonomy in admin
		 * @author  Mike Hemberger
		 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
		 */
		add_filter('parse_query', function( $query ){
			global $pagenow;

			$post_type = \AFAC_CM_Campaign::CPT; // change to your post type
			$taxonomy  = self::TAX; // change to your taxonomy
			$q_vars    = &$query->query_vars;
			if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {

				$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
				$q_vars[$taxonomy] = $term->slug;

			}
		});
	}

}