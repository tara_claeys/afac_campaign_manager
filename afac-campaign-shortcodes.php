<?php

add_shortcode( 'show_campaign_goal', 'afac_campaign_show_campaign_goal' );
	function afac_campaign_show_campaign_goal( $atts, $content ){

		$goal = get_post_meta( get_the_ID(), '_afac_campaign_goal', true );
		$raised = get_post_meta( get_the_ID(), '_afac_campaign_raised', true );
		if( ! $raised ) {
			$raised = (float) 0.0;
			update_post_meta( get_the_ID(), '_afac_campaign_raised', $raised );
		}
		if( empty( $goal ) ) {
			$progress = 0;
		} else {
			$progress = round( ( (float) $raised / (float) $goal ) * 180, 0 );
		}
		if ( $raised > $goal ) {
			$progress = 180;
		}

		ob_start();
//https://codepen.io/Sambego/pen/zKLar
?>
		<div class="campaign-goal-summary" >
			<div class="milestones" style="display: inline-block; width: calc( 50% - 105px );text-align: center;min-width:100px;">
				<h3>Our Goal:<br /><?php echo give_currency_filter( give_format_amount( $goal ) )?></h3>
			</div>
			<div class="box progress--gauge" data-progress="<?php echo $progress; ?>">
			    <div class="mask">
				<div class="semi-circle"></div>
				<div class="semi-circle--mask"></div>
			    </div>
			</div>
			<div class="milestones" style="display: inline-block; width: calc( 50% - 105px );text-align: center;min-width:100px;">
				<h3>Raised:<br /><?php echo give_currency_filter( give_format_amount( $raised ) )?></h3>
			</div>
		</div>
<?php
		return ob_get_clean();
	}


add_shortcode( 'show_donors', 'afac_campaign_show_campaign_donors' );
	function afac_campaign_show_campaign_donors( $atts, $content ){

		$defaults = array(
			'limit'		=> 5,
			'campaign'	=> ( is_singular( AFAC_CM_Campaign::CPT ) ) ? get_the_ID() : 0,
		);

		$args = shortcode_atts( $defaults, $atts, 'show_donors' );

		$output = '';
		if( $args['campaign'] ){
			$_payments = maybe_unserialize( get_post_meta( $args['campaign'], '_afac_donation_ids', true ) );

			$payments = give_get_payments( array(
				'meta_key'       => '_afac_campaign_id',
				'meta_value'     => $args['campaign'],
				'posts_per_page' => $args['limit'],
				'output'	=> 'payments'
			) );

			usort( $payments, function($a, $b){
				return strtotime($b->date) - strtotime($a->date);
			} );

			$output = '<div class="donor-list" style="display: table">';
			foreach( $payments as $payment ){

				$donation_amt = ( 'Yes' == $payment->get_meta( 'hide_donation_amount' ) ? '' : give_currency_filter( give_format_amount( $payment->total ) ) );

				$anonymous = $payment->get_meta('_give_anonymous_donation');
				if( $anonymous ){
					$donor_name = __('Anonymous', 'afac-cm' );
				} else {
				
					$donor_name = $payment->get_meta('donor_attribution');
					if( empty( $donor_name ) ) $donor_name = sprintf('%s %s',
						esc_html($payment->user_info['first_name']),
						esc_html($payment->user_info['last_name'])
					);
				}

				$output .= '<div style="display: table-row">';

				$output .= '<div class="donor-name" style="display: table-cell">';
				$output .= $donor_name;
				$output .= '</div>';

				$output .= '<div class="donation" style="display: table-cell">';
				$output .= sprintf('%s', $donation_amt );
				$output .= '</div>';

				$output .= '</div>';

			}
			$output .= '</div>';
		}

		return $output;
	}

/**
 * show-campaign-organizer 
 */
add_shortcode( 'show_campaign_organizer', function( $atts, $content ){

	ob_start();
	printf('<h3>Campaign Organizer: </h3>'); the_author();
?>
<br/><a href="mailto:<?php the_author_meta('email'); ?>">Contact the organizer</a>
<?php

	return ob_get_clean();
	
});


add_shortcode( 'show_campaign_enddate', function( $atts, $content ){

	ob_start();
	if( get_campaign_date() ):
?>
		<div class="campaign-dates">
		<h3>Campaign <? echo ( campaign_is_live() ? 'End Date' : 'Ended'); ?>:</h3>
		<?php the_campaign_date(); ?>
		</div><!-- CAMPAIGN DATES -->
<?php
	endif;
	return ob_get_clean();
	
});

add_shortcode( 'maybe_donation_form', function( $atts, $content ){

	if( campaign_is_live() ):
		
		ob_start();
		echo '<div class="give-form"><h3>DONATION INFORMATION:</h3>';
		echo do_shortcode( '[give_form id="7764" show_title="false" show_goal="false" display_style="reveal"]' );
		echo '</div>';
		return ob_get_clean();
		
	endif;
	
	return '';
});