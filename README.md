=== AFAC Campaign Manager Plugin ===
Contributors: transom, taraclaeys
Tags: givewp, fundraiser, fundraising, campaign, donation
Requires at least: 4.5.0
Tested up to: 4.5.2
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin allows the creation of Personal Fundraising Campaigns using the Give WordPress Donation Plugin.

== Description ==

A plugin that extends the Give plugin (https://givewp.com)to allow users to create their own fundraising campaigns on the front end of the website. Admin must approve the campaign, after which the user can share a link to their personal campaign to solicit donations from others. Donations are tracked in Give on a per-campaign basis.

This plugin was created specifically for the Arlington Food Assistance Center (AFAC).

Future development may include:
* Pledge campaigns
* Matching campaigns
* Integrated code to eliminate the need for ancillary plugins (Gravity Forms, CMB2)


== Installation ==
You need to have the Give WordPress plugin installed for the plugin to work. For front-end campaign creation, Gravity Forms and Gravity Forms+Custom Post Types is needed as well. Campaigns can be created on the backend without Gravity Forms. CMB2 and CMB2 Metatabs Options plugins are also required for this version.

= Where can I submit Support Questions? =
transom@1bigidea.com and tara@designtlc.com


== Changelog ==

= 1.0 =
* Initial release