<?php
/*
Plugin Name: AFAC Campaign Manager
Plugin URI: https://bitbucket.org/tara_claeys/afac_campaign_manager/
Description: Manage personal fundraising campaigns for AFAC
Version: 2.1
Author: Tom Ransom
Author URI: http://1bigidea.com
Network Only: false

Licensed under The MIT License (MIT)

Copyright 2015 Tom Ransom (email: transom@1bigidea.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/**
 * Basic PSR-4 Implementation.
 *
 * After registering this autoload function with SPL, the following line
 * would cause the function to attempt to load the StreamlineCore\Baz\Qux class
 * from /path/to/project/src/Baz/Qux.php:
 *
 *      new StreamlineCore\Baz\Qux;
 *
 * @param string $class The fully-qualified class name.
 * @return void
 */
spl_autoload_register(function ($class) {

    // project-specific namespace prefix
    $prefix = 'AFAC_CM\\';

    // base directory for the namespace prefix
    $base_dir = __DIR__ . '/src/';

    // does the class use the namespace prefix?
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        // no, move to the next registered autoloader
        return;
    }

    // get the relative class name
    $relative_class = substr($class, $len);

    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php (also change underlines to hyphens )
    $file = $base_dir . str_replace('\\', '/', str_replace( '_', '-', $relative_class ) ) . '.php';

    // if the file exists, require it
    if (file_exists($file)) {
        require $file;
    }
});

class AFAC_Campaign_Manager {

	/**
	 * @var instance variable.
	 */
	private static $_this;

	/**
	 * @var plugin_slug Slug of the Plugin.
	 */
	var $plugin_slug = 'AFAC_Campaign_Manager';
	/**
	 * @var class_prefix Identify dependent classes.
	 */
	var $class_prefix = 'AFAC_CM_';
	/**
	 * @var plugin_name Visible Name of plugin.
	 */
	var $plugin_name = 'AFAC Campaign Manager';
	/**
	 * @var string
	 */
	var $plugin_version = '2.1';

	/**
	 * AFAC_Campaign_Manager constructor.
	 */
	private function __construct() {

		$this->includes();

		// Catch the ajax donation.
		add_filter( 'give_donation_data_before_gateway', array( 'AFAC_CM_Campaign_Core', 'set_campaign_id' ), 10, 2 );
		new AFAC_CM_Payments();

		/*
		 * Dependent Plugin Activation/Deactivation
		 *
		 * Sources:
		 * 1. https://pippinsplugins.com/checking-dependent-plugin-active/
		 * 2. http://10up.com/blog/2012/wordpress-plug-in-self-deactivation/
		 *
		 */

		// If Parent Plugin is NOT active.
		if ( false and ( current_user_can( 'activate_plugins' ) && ! class_exists( 'Give' ) ) ) {

			add_action( 'admin_init', array( $this, 'campaign_deactivate' ) );
			add_action( 'admin_notices', array( $this, 'campaign_admin_notice' ) );


		} else {

			add_action( 'init', array( $this, 'init' ) );
			add_action( 'init', array( $this, 'maybe_flush_rewrite' ), 20 );

			AFAC_CM_Campaign::setup();
			//new AFAC_CM_Widgets();

			add_action( 'wp_head', function() {

				if ( is_singular( AFAC_CM_Campaign::CPT ) ) {
					AFAC_CM_Campaign::set_campaign_type();
				}
			} );

			add_filter( 'give_pdf_compiled_template_content', array( 'AFAC_CM_Notification', 'pdf_receipt' ), 10, 2 );

		}

	}

	// Deactivate the Child Plugin
	/**
	 *
	 */
	public function campaign_deactivate() {
		deactivate_plugins( plugin_basename( __FILE__ ) );
	}

	// Throw an Alert to tell the Admin why it didn't activate
	/**
	 *
	 */
	public function campaign_admin_notice() {

		$dpa_child_plugin = __( 'AFAC Campaign Manager', 'afac_cm' );
		$dpa_parent_plugin = __( 'Give', 'afac_cm' );

		echo '<div class="error"><p>'
			. sprintf( __( '%1$s requires %2$s to function correctly. Please activate %2$s before activating %1$s. For now, the plugin has been deactivated.', 'afac_cm' ), '<strong>' . esc_html( $dpa_child_plugin ) . '</strong>', '<strong>' . esc_html( $dpa_parent_plugin ) . '</strong>' )
			. '</p></div>';

	   if ( isset( $_GET['activate'] ) )
			unset( $_GET['activate'] );
	}

	/**
	 *
	 */
	function __destruct(){
        /* flush_rewrite_rules(); */
	}

	/**
	 *
	 */
	public static function activate() {
		// Add options, initiate cron jobs here.
		if ( ! get_option( 'afac_campaign_flush_rewrite_rules_flag' ) ) {
			add_option( 'afac_campaign_flush_rewrite_rules_flag', true );
		}
	}
	/**
	 * Flush rewrite rules if the previously added flag exists,
	 * and then remove the flag.
	 *
	 * http://www.andrezrv.com/2014/08/12/efficiently-flush-rewrite-rules-plugin-activation/
	 */
	function maybe_flush_rewrite() {

		if ( get_option( 'afac_campaign_flush_rewrite_rules_flag' ) ) {
			flush_rewrite_rules();
			delete_option( 'afac_campaign_flush_rewrite_rules_flag' );
		}

	}

	/**
	 *
	 */
	public static function uninstall() {
		// Delete options here
	}

	/**
	 * @return AFAC_Campaign_Manager|instance
	 */
	static function get_instance(){
		// enables external management of filters/actions
		// http://hardcorewp.com/2012/enabling-action-and-filter-hook-removal-from-class-based-wordpress-plugins/
		// enables external management of filters/actions
		// http://hardcorewp.com/2012/enabling-action-and-filter-hook-removal-from-class-based-wordpress-plugins/
		// http://7php.com/how-to-code-a-singleton-design-pattern-in-php-5/
		if( !is_object(self::$_this) ) self::$_this = new self();

		return self::$_this;
	}

	/**
	 *
	 */
	function includes(){

		require_once( 'afac-campaign-template-tags.php' );
		require_once( 'afac-campaign-shortcodes.php' );
		require_once( 'includes/class-afac-cm-admin-settings.php' );

		// supporting classes
		require_once( 'includes/class-afac-cm-admin.php' );
		require_once( 'includes/class-afac-cm-settings.php' );
		require_once( 'includes/class-afac-campaign.php' );

		require_once( 'includes/class-afac-cm-campaign.php' );
		require_once( 'includes/class-afac-cm-donors.php' );
		require_once( 'includes/class-afac-cm-notification.php' );
		require_once( 'includes/class-afac-cm-payments.php' );
		require_once( 'includes/class-afac-cm-widgets.php' );
		require_once( 'includes/class-afac-cm-widget-featured-campaign.php' );

		// Campaign Types stuff
		require_once( 'includes/class-afac-cm-campaign-core.php' );
		require_once( 'includes/class-afac-cm-campaign-fundraiser.php' );
		require_once( 'includes/class-afac-cm-campaign-match.php' );
		require_once( 'includes/class-afac-cm-campaign-pledge.php' );


	}
	/**
	 *	Functions below actually do the work
	 */
	public function init(){

		if( version_compare ( get_option('afac_cm_version', 0.0 ), $this->plugin_version ) === -1 ) {
			new AFAC_CM\Upgrade;
			update_option( 'afac_cm_version', $this->plugin_version ); // bump db version to latest
		}

		add_action( 'admin_init', array( $this, 'afac_cm_settings' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_stuff' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_stuff' ) );

		add_action( 'admin_menu', array( $this, 'setting_pages' ) );


	}

	public function setting_pages(){
		new AFAC_CM_Admin_Settings();
	}

	/**
	 *
	 */
	public function afac_cm_settings(){
		/**
		 * Register our setting to WP
		 * @since  0.1.0
		 */
		register_setting( AFAC_CM_Settings::$key, AFAC_CM_Settings::$key );

	}

	/**
	 * @param $file
	 *
	 * @return string
	 */
	public function plugins_url( $file ){
		return plugins_url( $file, __FILE__ );
	}

	/**
	 *
	 */
	public function enqueue_stuff(){

		wp_enqueue_script( 'afac-gauge', plugins_url( '/assets/js/afac-gauge.js', __FILE__ ), array('jquery') );
//		wp_enqueue_style( 'afac-gauge', plugins_url( '/assets/css/afac-gauge.css', __FILE__ ) );

		wp_enqueue_style( 'afac-gauge-2', plugins_url( '/assets/dist/css/afac-gauge-pure.css', __FILE__ ) );
	}

	public function enqueue_admin_stuff(){

		wp_enqueue_style( 'afac-admin', plugins_url( '/assets/css/afac-cm-admin.css', __FILE__ ) );

	}
	
}

/**
 * @return AFAC_Campaign_Manager
 */
function afac_campaign(){
	return AFAC_Campaign_Manager::get_instance();
}
register_activation_hook(   __FILE__, array( 'AFAC_Campaign_Manager', 'activate'   ) );

register_deactivation_hook( __FILE__, 'afac_campaign_manager_deactivate' );
function afac_campaign_manager_deactivate(){ }

register_uninstall_hook(    __FILE__, 'afac_campaign_manager_uninstall' );
function afac_campaign_manager_uninstall(){ }

add_action( 'plugins_loaded', array( 'AFAC_Campaign_Manager', 'get_instance' ) );
