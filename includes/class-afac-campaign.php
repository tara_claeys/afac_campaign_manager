<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class AFAC_Campaign {

	public function __construct( $campaign_id ){

		$campaign = new AFAC_Campaign_Query( array(
			'p'	=> $campaign_id,
			'post_status' => array('any'),
		) );

		if( $campaign ) { $this->setup_campaign( $campaign->post ); }
	}

	private function setup_campaign( $campaign ){

		if( ! is_object( $campaign ) ) return false;

		foreach( $campaign as $key => $value ){

			switch ( $key )
			{
			    case 'post_author':
			        $this->organizer = get_the_author_meta( 'nicename', $value );
			        $this->organizer_email = get_the_author_meta( 'email', $value );
			        break;
			    default:
			        $this->{$key} = $value;
			        break;
			}

		}

		$this->url = get_permalink( $campaign->ID );

		$meta = get_post_meta( $campaign->ID );
		foreach( $meta as $key => $values ){

			switch( $key ) {

				default:
					if( '_afac_campaign_' == substr( $key, 0, 15 ) ) {

						$newkey = substr( $key, 15 );
						$this->{$newkey} = maybe_unserialize( $values[0] );

					}
					break;
			}
		}

	}

	public function get_donations(){
		$donations = get_post_meta( $this->ID, '_afac_donation', false );

		if( empty($donations) ) $donations = array();

		$this->donations = $donations;
	}

	public static function setup_placeholders(){

		$values = array(
			'title'	    => __( 'Campaign Title', 'afac_cm' ),
			'organizer'	=> __( 'Campaign Organizer Name', 'afac_cm' ),
			'email'     => __( 'Campaign Organizer Email', 'afac_cm' ),
			'donation'	=> __( 'Amount of the donation', 'afac_cm' ),
			'donor'     => __( 'Donor Name', 'afac_cm' ),
			'goal'		=> __( 'Campaign Goal', 'afac_cm' ),
			'raised'	=> __( 'Total Raised-to-date', 'afac_cm' ),
			'url'		=> __( 'Url to Campaign page', 'afac_cm' ),
			'end_date'  => __( 'Campaign Ending Date', 'afac_cm' ),
		);

		return $values;

	}

	public function get_email_args(){

		$args = array();

		$args['title']      = esc_html( $this->post_title );
		$args['organizer']  = esc_html( $this->organizer );
		$args['email']      = esc_html( $this->organizer_email );
		$args['goal']       = ( isset( $this->goal ) ? give_format_amount( $this->goal ) : '$0.00' );
		if( isset( $this->raised ) ) {
			$args['raised'] = give_format_amount( $this->raised );
		} else {
			$args['raised'] = '$0.00';
		}
		$args['url']        = $this->url;
		$args['end_date']   = $this->end_date;

		return $args;
	}

	public function has_organizer_been_notified_on_publish(){

		// we already sent the "published" email to the organizer.
		if( isset( $this->published ) && 1 == $this->published ) return true;

		return false;

	}

	public function organizer_notified_on_publish(){
		update_post_meta( $this->ID, '_afac_campaign_published', 1 );
	}

}
