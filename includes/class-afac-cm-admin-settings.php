<?php

class AFAC_CM_Admin_Settings {

	public static $options_key = 'afac_cm_options';

	function __construct(){

		$this->setup_general_settings_page();

	}

	private function setup_general_settings_page(){

		// use CMO filter to add an intro at the top of the options page
		add_filter( 'cmb2metatabs_before_form', array( $this, 'intro_via_filter' ) );

		// configuration array
		$args = array(
			'key'     => self::$options_key,
			'title'   => 'AFAC CM Settings',
			'topmenu'   => 'edit.php',
			'postslug'  => AFAC_CM_Campaign::CPT,
			'cols'    => 2,
			'boxes'	  => $this->add_metaboxes( ),
			'tabs'	  => $this->add_tabs(),
		);

		// create the options page
		new Cmb2_Metatabs_Options( $args );
	}

	/**
	 * Callback for CMO filter.
	 *
	 * The two filters in CMO do not send any content; simply return your HTML.
	 *
	 * @return string
	 */
	function intro_via_filter() {
		return '<p>Use this page to set the options for the AFAC Campaign Manager</p>';
	}

	/**
	 * Add the options metabox to the array of metaboxes
	 * @since  0.1.0
	 */
	public function add_metaboxes( ) {

		// holds all CMB2 box objects
		$boxes = array();

		// we will be adding this to all boxes
		$show_on = array(
			'key' => 'options-page',
			'value' => array( self::$options_key ),
		);

		// first box
		$cmb = new_cmb2_box( array(
			'id'        => 'give_form',
			'title'     => __( 'Give Form for Personal Campaigns', 'afac-cm' ),
			'show_on'   => $show_on, // critical, see wiki for why
		));

		// Get a list of Give Forms
		$forms = new WP_Query( array(
			'post_type'			=> 'give_forms',
			'post_status'		=> 'publish',
			'posts_per_page'	=> -1,
		) );

		$forms_list = array();
		if( $forms->have_posts() ){
			while( $forms->have_posts() ) : $forms->the_post();

				$forms_list[ get_the_ID() ] = get_the_title();

			endwhile;
			wp_reset_query();
		}

		$cmb->add_field( array(
			'name'             => __( 'Campaign Donation Form', 'afac_cm' ),
			'desc'             => __( 'Must already exist as Give Form', 'afac_cm' ),
			'id'               => 'donation_form',
			'type'             => 'select',
			'show_option_none' => true,
			'options'          => $forms_list,
		) );

		$cmb->add_field( array(
			'name'              => __( 'Days before Campaign End', 'afac_cm' ),
			'desc'              => __( 'Send email this many days before campaign ends', 'afac_cm' ),
			'id'                => 'days_before_end',
			'type'              => 'text_small'

		) );

		$cmb->object_type( 'options-page' );  // critical, see wiki for why
		$boxes[] = $cmb;

		$fields = array(

			array(
				'name'    => __( 'Subject', 'afac_cm' ),
				'desc'    => __( 'Subject for the email', 'afac_cm' ),
				'id'      => 'subject',
				'type'    => 'text',
			),

			array(
				'name'    => __( 'From (Name)', 'afac_cm' ),
				'desc'    => __( 'Name of Sender', 'afac_cm' ),
				'id'      => 'from_name',
				'type'    => 'text',
			),

			array(
				'name'    => __( 'From (email)', 'afac_cm' ),
				'desc'    => __( 'Sender Email Address (afac.org)', 'afac_cm' ),
				'id'      => 'from_email',
				'type'    => 'text_email',
			),

			array(
				'name'    => __( 'Email Body', 'afac_cm' ),
				'desc'    => __( 'Body of email', 'afac_cm' ),
				'id'      => 'body',
				'type'    => 'wysiwyg',
				'options' => array( 'textarea_rows' => 5 ),
			),

		);

		$index = 0;
		foreach( $this->the_emails() as $email_id => $email_title ) :

			$cmb = new_cmb2_box( array(
				'id'        => $email_id,
				'title'     => $email_title,
				'show_on'   => $show_on,
			));

			foreach ( $fields as $email_fields ) :
				$the_fields = $email_fields;
				$the_fields['id'] = $email_id . '_' .$the_fields['id'];
				$cmb->add_field( $the_fields );
			endforeach;

			$cmb->object_type( 'options-page' );
			$boxes[] = $cmb;

		endforeach;

		$cmb = new_cmb2_box( array(
			'id'    => 'placeholders',
			'title' => __( 'Email Placeholders', 'afac-cm' ),
			'show_on'   => $show_on,
			'context'   => 'side'
		) );

		$placeholders = AFAC_Campaign::setup_placeholders();
		$copy = __( 'Create Email Templates for AFAC Campaign actions. Use these placeholders for common values:', 'afac-cm' );
		foreach( $placeholders as $key => $label ){
			$copy .= sprintf( '<br />{%s} - %s', $key, $label );
		}

		$cmb->add_field( array(
			'name'  => __('Using Placeholders', 'afac_cm' ),
			'desc'	=> $copy,
			'id'    => 'placeholders',
			'type'  => 'title'
		) );
		$cmb->object_type( 'options-page' );
		$boxes[] = $cmb;

		return $boxes;
	}

	function add_tabs() {

		$tabs = array();

		$tabs[] = array(
			'id'    => 'general',
			'title' => 'General',
			'desc'  => '<p>General Settings</p>',
			'boxes' => array(
				'give_form',
			),
		);
		$tabs[] = array(
			'id'    => 'emails',
			'title' => 'Emails',
			'desc'  => 'Templates for Personal Fundraising transactions',
			'boxes' => array_keys( $this->the_emails() ),
		);

		return $tabs;
	}

	/**
	 * Defines the theme option metabox and field configuration
	 *
	 * @since  0.1.0
	 * @return array
	 */
	public function the_emails() {

		return array(
			'campaign_approved' => __("Campaign Approved", 'afac_cm' ),
			'goal_reached'		=> __("Goal Reached", 'afac_cm' ),
			'donation_received'	=> __("Donation Received", 'afac_cm' ),
			'campaign_ending'	=> __("Campaign Ending", 'afac_cm' ),
		);

	}

	public static function get_option(){

		return get_option( self::$options_key );
	}

}

function afac_cm_get_option(){

	return AFAC_CM_Admin_Settings::get_option();
}
