<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly.

add_action('give_insert_payment', array('AFAC_CM_Payments', 'add_campaign_meta'), 10, 2);
add_action('give_update_payment_status', array('AFAC_CM_Payments', 'record_donation'), 10, 3);

add_filter('give_payments_table_columns', array('AFAC_CM_Payments', 'payments_table_columns'));
add_filter('give_payments_table_column', array('AFAC_CM_Payments', 'payments_table_campaign_column'), 10, 3);

add_action('give_view_donation_details_billing_before', array('AFAC_CM_Payments', 'payment_detail'), 10, 1);

// See if the campaign_id changed
add_action('give_update_edited_donation', array('AFAC_CM_Payments', 'update_payment'), 12);


class AFAC_CM_Payments
{

    public static function add_campaign_meta($payment, $payment_data)
    {
        if (isset($payment_data['user_info']['campaign_id']))
            give_update_payment_meta($payment, '_afac_campaign_id', $payment_data['user_info']['campaign_id']);
    }


    public static function record_donation($payment_id, $new_status, $old_status)
    {

        if ('publish' == $new_status && !('publish' == $old_status)) :

            $payment = new Give_Payment($payment_id);
            $campaign_id = $payment->get_meta('_afac_campaign_id');

            if (false != $campaign_id) {
                AFAC_CM_Donors::record_donation($payment_id);
            }

        endif;
    }

    public static function payments_table_columns($columns)
    {

        $campaign_col = array('campaign' => __('Campaign', 'afac_cm'));

        $new_columns = array_slice($columns, 0, (count($columns) - 2), true) + $campaign_col + array_slice($columns, (count($columns) - 1), null, true);

        return $new_columns;
    }

    public static function payments_table_campaign_column($value, $payment_id, $column_name)
    {

        if ('campaign' != $column_name) {
            return $value;
        }

        $camp_id = get_post_meta($payment_id, '_afac_campaign_id', true);
        if (!$camp_id) {
            return $value;
        }

        $campaign = new AFAC_Campaign($camp_id);
        if (!is_object($campaign) || !isset($campaign->ID)) {
            return $value;
        }

        return esc_html($campaign->post_title);
    }

    public static function payment_detail($payment_id)
    {

        $camp_id = get_post_meta($payment_id, '_afac_campaign_id', true);
        $personal_campaign_view = 'is-personal-campaign';
        if ($camp_id) {
            $campaign = new AFAC_Campaign($camp_id);
            $personal_campaign_view = '';
        }

        $campaigns = new AFAC_Campaign_Query(); // get a list of campaigns
        $detail_page = '';
        if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
            $page_url = get_post_meta($_REQUEST['id'], '_give_current_url', true);
            $detail_page = sprintf('<p>Donation Page: %s</p>', $page_url);
        }

        if (!$campaigns->have_posts()) return;
?>
        <div id="afac-campaign" class="postbox">
            <h3 class="hndle">
                <span><?php _e('Personal Fundraiser', 'afac_cm'); ?></span>
            </h3>

            <div class="inside">
                <?php echo $detail_page; ?>
                <a href="#" class="button select-personal-campaign <?php echo $personal_campaign_view; ?>">Assign to Personal Campaign</a>

                <table style="width:100%;text-align:left;" class="<?php echo $personal_campaign_view; ?> choose-personal-campaign">
                    <thead>
                        <tr>
                            <?php do_action('afac_campaign_details_thead_before', $payment_id, $camp_id); ?>
                            <th><?php _e('Campaign', 'give') ?></th>
                            <?php do_action('afac_campaign_details_thead_after', $payment_id, $camp_id); ?>
                        </tr>
                    </thead>
                    <tr>
                        <?php do_action('afac_campaign_details_tbody_before', $payment_id, $camp_id); ?>
                        <td>
                            <select class="" name="afac_personal_campaign">
                                <option value=0><?php _e('Select Campaign...', 'afac-campaign-manager'); ?></option>
                                <?php
                                while ($campaigns->have_posts()) : $campaigns->the_post();
                                    printf('<option value=%d %s>%s</option>', get_the_ID(), selected(get_the_ID(), $camp_id, false), get_the_title());
                                endwhile;
                                wp_reset_postdata();
                                ?>
                            </select>
                        </td>
                        <?php do_action('afac_campaign_details_tbody_after', $payment_id, $camp_id); ?>

                    </tr>
                </table>
            </div>
            <!-- /.inside -->

            <br />
            <script>
                jQuery(function($) {
                    $('#afac-campaign .select-personal-campaign').on('click', function(e) {
                        console.log('hello world');
                        $('#afac-campaign .choose-personal-campaign').show();
                        //						$('#afac-campaign select').addClass('give-select give-select-chosen');
                        $('#afac-campaign .select-personal-campaign').hide();
                    });
                });
            </script>
        </div>
        <!-- /#afac-campaign -->
<?php

    }

    /**
     * Update Campaign Data after editing Transaction
     */
    public static function update_payment($payment_id)
    {
        $data = $_POST; // WARNING - UNsanitized

        // the possibilities -
        // general donation -> applied to Campaign
        // Campaign donation -> should be just general
        // Changing donation from one campaign to another

        // Retrieve the payment ID
        $payment_id = absint($data['give_payment_id']);
        $new_camp_id = absint($data['afac_personal_campaign']);
        $was_camp_id = get_post_meta($payment_id, '_afac_campaign_id', true);
        if (false === $was_camp_id || empty($was_camp_id)) $was_camp_id = 0; // this was a general donation

        if ($new_camp_id == $was_camp_id) {
            // No change
            return;
        } elseif (0 == $was_camp_id) {
            // general campaign becomes personal campaign donation
            update_post_meta($payment_id, '_afac_campaign_id', $new_camp_id);
            AFAC_CM_Donors::record_donation($payment_id, $notify = false);
        } elseif (0 == $new_camp_id) {
            // personal campaign donation becomes general donation
            $donation = AFAC_CM_Donors::revert_donation($payment_id, $was_camp_id);
            delete_post_meta($payment_id, '_afac_campaign_id', $was_camp_id);
        } else {
            // switching personal campaigns
            $donation = AFAC_CM_Donors::revert_donation($payment_id, $was_camp_id);
            update_post_meta($payment_id, '_afac_campaign_id', $new_camp_id);
            AFAC_CM_Donors::record_donation($payment_id, $notify = false);
        }

        return;

        // Otherwise - we need to update both campaigns
        $old = new AFAC_Campaign($was_camp_id);
        // decrement the donation from the totals

        // remove the payment_id from the donations

        // Update the _afac_donation record (attached to campaign - to the new campaign )
        // This would have been easier if I had added the payment_id originally so look for payment_id first,
        // otherwise, use the email of the payment and timestamp to find the right donation record

        $new = new AFAC_Campaign($new_camp_id);
        // Increment the funds raised
        // Add the payment_id to the donations

        // Update the _afac_campaign_id of the $payment_id



    }
}
