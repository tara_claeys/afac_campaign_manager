<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class AFAC_CM_Campaign_Core {

	function __construct(){

		add_action( 'give_donation_form_bottom', array( $this, 'link_donation_to_campaign' ), 10, 2 );

	}

	function link_donation_to_campaign( $form_ID, $args ){

		if( is_singular( AFAC_CM_Campaign::CPT ) ) :
			printf( '<input type="hidden" name="afac-campaign" value="%d" />', get_the_ID() );
		endif;

	}

	public static function set_campaign_id( $purchase_data, $valid_data ){

		if( isset( $purchase_data['post_data'] ) and isset( $purchase_data['post_data']['afac-campaign'] ) ){

			$post_type = get_post_field('post_type', (int) $purchase_data['post_data']['afac-campaign'] );
			if( AFAC_CM_Campaign::CPT == $post_type ){
				$purchase_data['user_info']['campaign_id'] = (int) $purchase_data['post_data']['afac-campaign'];
			}
		}

		return $purchase_data;
	}

	public function get_campaign_goal( $form_goal, $form_id ){

		if( is_singular( AFAC_CM_Campaign::CPT ) ) {

			$goal = get_post_meta( get_the_ID(), '_afac_campaign_goal', true );
			if( ! $goal ) $goal = $form_goal;
			return give_sanitize_amount( $goal );
		}

		return $form_goal;
	}

}