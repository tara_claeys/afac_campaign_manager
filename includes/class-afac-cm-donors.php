<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class AFAC_CM_Donors {

	public static function get_donors_by_campaign( $campaign = 0 ){

		//Get donors based on forms IDs (can be single or multiple forms)
		// lifted from https://givewp.com/documentation/developers/how-to-query-donor-information/

		$args = array(
			'give_forms' => array(8, 888),
		);

		$donations = give_get_payments( $args );
		foreach( $donations as $donation ) {

			//Now get donor information from this donation ("customer" aka "donor")
			$customer_id = give_get_payment_customer_id( $donation->ID );
			$customer    = new Give_Customer( $customer_id );

			//Here's the customer object returned with all sorts of useful data
			echo "<pre>";
			var_dump($customer);
			echo "</pre>";

		}
	}

	public static function record_donation( $payment_id, $notify = true ){

		$payment = new Give_Payment( $payment_id );
		$campaign_id = $payment->get_meta('_afac_campaign_id');
		$campaign = new AFAC_Campaign( $campaign_id );


		if( ! isset( $campaign->donation_ids ) ) $campaign->donation_ids = array(); // no donations recorded yet

		if( ! in_array ( $payment_id, $campaign->donation_ids ) ){
			// insert donation as meta for campaign
			$details = $payment->get_meta();
			$details['amount'] = $payment->get_meta( '_give_payment_total' );
			$details['payment_id'] = $payment_id;
			add_post_meta( $campaign_id, '_afac_donation', $details, false );
			$campaign->donation_ids[] = $payment_id;
			update_post_meta( $campaign->ID, '_afac_campaign_donation_ids', $campaign->donation_ids );

			self::update_funding( $campaign, $details['amount'] );

			if( $notify ) self::notify_organizer( $campaign, $payment_id );

		}
	}

	public static function revert_donation( $payment_id, $campaign_id ){

		$campaign = new AFAC_Campaign( $campaign_id );
		$payment    = new Give_Payment( $payment_id );

		// do we have this donation recorded in the campaign
		if( ! isset( $campaign->donation_ids ) || ! in_array( $payment_id, $campaign->donation_ids ) ) return;

		// Yes we do
		// Get all the donation records for this campaign
		$donations = get_post_meta( $campaign_id, '_afac_donation', false );
		foreach( $donations as $donation ){
			if( $payment->key != $donation['key'] ) continue;

			delete_post_meta( $campaign_id, '_afac_donation', $donation );
			// Remove the donation from the campaign keys
			$key = array_search ( $payment_id, $campaign->donation_ids );
			if( ! ( false === $key ) ){
				unset( $campaign->donation_ids[$key] );
				update_post_meta( $campaign->ID, '_afac_campaign_donation_ids', $campaign->donation_ids );
			}
			self::update_funding( $campaign, -1 * $donation['amount'] );

			return $donation;
		}
	}

	public static function update_funding( &$campaign, $amount ){

			// Update funding to date
			if( ! isset( $campaign->raised ) ) $campaign->raised = (float) 0.0;
			$campaign->raised += (float) $amount;
			update_post_meta( $campaign->ID, '_afac_campaign_raised', $campaign->raised );

	}

	private static function notify_organizer( $campaign, $payment_id ){

		$payment = new Give_Payment( $payment_id );

		$payment_details = $payment->get_meta();
		$donation = $payment->get_meta('_give_payment_total');
		$anonymous = $payment->get_meta('_give_anonymous_donation');

		if( $anonymous ){
			$donor_full_name = __('Anonymous', 'afac-cm' );
		} else {
			$donor_full_name = sprintf( '%s %s', $payment_details['user_info']['first_name'], $payment_details['user_info']['last_name'] );		
		}

		// Notify organizer
		$mail = new AFAC_CM_Notification( 'donation_received' );

		if( is_object( $mail ) ) {

			$mail->to( $campaign->organizer_email, $campaign->organizer );

			$args = array(
				'donation'  => give_currency_filter( give_format_amount( $donation ) ),
				'donor'     => $donor_full_name,
				'donor_first'   => $payment_details['user_info']['first_name'],
			);

			$mail->set_placeholders( array_merge( $args, $campaign->get_email_args() ) );

			$mail->send();

		}

	}
}
