<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class AFAC_CM_Widgets {

	function __construct(){

		add_action( 'widgets_init', array( __CLASS__, 'register_widgets' ) );

	}

	public static function register_widgets(){

		register_widget( 'AFAC_CM_Widget_Featured_Campaign' );
	}

}