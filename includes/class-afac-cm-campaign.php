<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	/**
	 * Class AFAC_CM_Campaign
	 */
	class AFAC_CM_Campaign {

		/**
		 *
		 */
		const CPT = 'afac-campaign';

		/**
		 *
		 */
		public static function setup(){

			add_action( 'init', array( 'AFAC_CM_Campaign', 'register_cpt' ) );
			add_action( 'cmb2_admin_init', array( 'AFAC_CM_Campaign', 'register_metaboxes' ) );
			add_action( 'publish_' . self::CPT, array( 'AFAC_CM_Campaign', 'campaign_approved' ), 10, 2 );
			add_filter( 'give_email_tags', array( 'AFAC_CM_Notification', 'add_email_tags' ), 15 );

		}

	// Register Custom Post Type
		/**
		 *
		 */
		public static function register_cpt() {

		$labels = array(
			'name'                  => _x( 'Campaigns', 'Post Type General Name', 'afac_cm' ),
			'singular_name'         => _x( 'Campaign', 'Post Type Singular Name', 'afac_cm' ),
			'menu_name'             => __( 'Campaigns', 'afac_cm' ),
			'name_admin_bar'        => __( 'Campaigns', 'afac_cm' ),
			'archives'              => __( 'Campaign Archives', 'afac_cm' ),
			'parent_item_colon'     => __( 'Parent Campaign:', 'afac_cm' ),
			'all_items'             => __( 'All Campaigns', 'afac_cm' ),
			'add_new_item'          => __( 'Add New Campaign', 'afac_cm' ),
			'add_new'               => __( 'Add New', 'afac_cm' ),
			'new_item'              => __( 'New Campaign', 'afac_cm' ),
			'edit_item'             => __( 'Edit Campaign', 'afac_cm' ),
			'update_item'           => __( 'Update Campaign', 'afac_cm' ),
			'view_item'             => __( 'View Campaign', 'afac_cm' ),
			'search_items'          => __( 'Search Campaign', 'afac_cm' ),
			'not_found'             => __( 'Not found', 'afac_cm' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'afac_cm' ),
			'featured_image'        => __( 'Featured Image', 'afac_cm' ),
			'set_featured_image'    => __( 'Set featured image', 'afac_cm' ),
			'remove_featured_image' => __( 'Remove featured image', 'afac_cm' ),
			'use_featured_image'    => __( 'Use as featured image', 'afac_cm' ),
			'insert_into_item'      => __( 'Insert into campaign', 'afac_cm' ),
			'uploaded_to_this_item' => __( 'Uploaded to this campaign', 'afac_cm' ),
			'items_list'            => __( 'Campaigns list', 'afac_cm' ),
			'items_list_navigation' => __( 'Campaigns list navigation', 'afac_cm' ),
			'filter_items_list'     => __( 'Filter campaigns list', 'afac_cm' ),
		);
		$rewrite = array(
			'slug'                  => 'campaign',
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => true,
		);
		$args = array(
			'label'                 => __( 'Campaign', 'afac_cm' ),
			'description'           => __( 'Personal Fundraising Campaigns', 'afac_cm' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'author', 'thumbnail', ),
			'taxonomies'            => array( 'afac-campaign-types' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 25,
			'menu_icon'             => afac_campaign()->plugins_url('/assets/images/dollar_sign.png'),
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'afac-campaigns',
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
		);

		register_post_type( self::CPT, $args );

		new \AFAC_CM\Taxonomies\CampaignCats;
		new \AFAC_CM\Taxonomies\CampaignTags;

		add_action( 'load-edit.php', function() {

		    $screen = get_current_screen();

		    if ( ! isset ( $screen->id ) ) return;

		    if ( "edit-".self::CPT !== $screen->id ) return;

			$new_columns = array(
				'expires'		=> __( 'Expires', 'afac_cm' ),
				'donations'	=> __( 'Donations', 'afac_cm' ),
			);
			$insert_offset_column = 2;

			add_filter( "manage_".self::CPT."_posts_columns", function( $posts_columns ) use ( $new_columns, $insert_offset_column ){
		    		$new = self::array_insert( $posts_columns, $insert_offset_column, $new_columns );
		    		return $new;
			});

			add_action( "manage_".self::CPT."_posts_custom_column", function( $column_name, $post_id ) use ( $new_columns ) {
			
				  if ( ! in_array ( $column_name, array_keys( $new_columns ) ) ) return;
				  
				  $UTC = new DateTimeZone('UTC');
				  switch ( $column_name )
				  {
				      case "expires":
				      	$end_date = get_post_meta( $post_id, '_afac_campaign_end_date', true );

				      	if( empty( get_post_meta( $post_id, '_afac_campaign_end_date', true ) ) ){
				      		$exp = __( 'Never', 'afac_cm_campaign' );
				      	} elseif( isValidTimeStamp_afac( $end_date ) ) {
							$expires = new DateTime( '@'.$end_date, $UTC );
							$exp = $expires->format('m/d/y');
						}else{
							$expires = new DateTime( $end_date, $UTC );
							$exp = $expires->format('m/d/y');
						}

						$output = sprintf( '%s %s %s', 
							( campaign_is_live( $post_id ) ? '' : '<strike>' ),
							$exp,
							( campaign_is_live( $post_id ) ? '' : '</strike>' )
						);

						echo $output;
				          break;
					
					case 'donations':
					
						$payments = give_get_payments( array(
							'meta_key'       => '_afac_campaign_id',
							'meta_value'     => $post_id,
							'posts_per_page' => -1,
							'output'	=> 'payments'
						) );
						$total = $count = 0;
						foreach( $payments as $payment ){
							$total += $payment->total;
							$count++;
						}
						printf( '%s (%d)', give_currency_filter( give_format_amount( $total ) ), $count );
						break;
						
				      default:
				          
				          break;
				  }
				  

			}, 10, 2 );
			
			add_filter( 'manage_edit-'.self::CPT.'_sortable_columns', function( $columns ){

			    $columns['expires'] = 'expires';
 
			    //To make a column 'un-sortable' remove it from the array
			    //unset($columns['date']);
 
			    return $columns;
			
			});

			add_action( 'pre_get_posts', function( $query ){
			
			    if( ! is_admin() )
				   return;
 
			    $orderby = $query->get( 'orderby');
 
			    if( 'expires' == $orderby ) {
				   $query->set('meta_key','_afac_campaign_end_date');
				   $query->set('orderby','meta_value_num');
			    }

			} );

		});
	}
	
	private static function array_insert( $array, $index, $insert ) {
		return array_slice( $array, 0, $index, true ) + $insert + array_slice( $array, $index, count( $array ) - $index, true);
	}
	


		/**
		 *
		 */
		public static function register_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_afac_campaign_';

		/**
		 * Sample metabox to demonstrate each field type included
		 */
		$campaign_metabox = new_cmb2_box( array(
			'id'            => $prefix . 'metabox',
			'title'         => __( 'Campaign Details', 'afac_cm' ),
			'object_types'  => array( self::CPT, ), // Post type
			// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
			// 'context'    => 'normal',
			// 'priority'   => 'high',
			// 'show_names' => true, // Show field names on the left
			// 'cmb_styles' => false, // false to disable the CMB stylesheet
			// 'closed'     => true, // true to keep the metabox closed by default
		) );

		$campaign_metabox->add_field( array(
			'name'	=> __( 'Fundraising Organization', 'afac_cm' ),
			'desc'	=> __( 'Sponsoring Organization', 'afac_cm' ),
			'id'	=> $prefix . 'sponsor_org',
			'type'	=> 'text'
		) );

		$campaign_metabox->add_field( array(
			'name' => __( 'End Date', 'afac_cm' ),
			'desc' => __( 'When does campaign end? (optional)', 'afac_cm' ),
			'id'   => $prefix . 'end_date',
			'type' => 'text_date_timestamp',
		) );

		$campaign_metabox->add_field( array(
			'name' => __( 'Goal', 'afac_cm' ),
			'desc' => __( 'How much do you want to raise? (optional)', 'afac_cm' ),
			'id'   => $prefix . 'goal',
			'type' => 'text_money',
			'sanitization_cb' => array( __CLASS__, 'parsefloat' ),
			// 'before_field' => '£', // override '$' symbol if needed
			// 'repeatable' => true,
		) );

		$campaign_featured = new_cmb2_box( array(
			'id'            => $prefix . 'featured_metabox',
			'title'         => __( 'Featured Campaign', 'afac_cm' ),
			'object_types'  => array( self::CPT, ), // Post type
			'context'		=> 'side',
			'priority'		=> 'high'
		) );

		$campaign_featured->add_field( array(
			'name' => __( 'Check if Featured', 'afac_cm' ),
			'desc' => __( 'available as featured campaign', 'afac_cm' ),
			'id'   => $prefix . 'featured',
			'type' => 'checkbox',
		) );

	}
		public static function parsefloat( $field ){
			return filter_var( $field, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
		}

		/**
		 *
		 */
		public static function set_campaign_type(){

		new AFAC_CM_Campaign_Fundraiser;

	}

		/**
		 * Record that post_status of campaign has been changed to published.
		 *
		 * @param $ID  int ID of the post that was publishd
		 * @param $post object $post object
		 */
		public static function campaign_approved( $ID, $post ) {

			if( AFAC_CM_Campaign::CPT != $post->post_type ) { return; }

			$campaign = new AFAC_Campaign( $ID );

			if( $campaign->has_organizer_been_notified_on_publish() ) { return; }

			$mail = new AFAC_CM_Notification( 'campaign_approved' );

			if( is_object( $mail ) ) {

				$mail->to( $campaign->organizer_email, $campaign->organizer );

				$mail->set_placeholders( $campaign->get_email_args() );

				$mail->send();

				$campaign->organizer_notified_on_publish();
			}

	}
	
}

	/**
	 * Class AFAC_Campaign_Query
	 */
	class AFAC_Campaign_Query extends WP_Query {

		/**
		 * AFAC_Campaign_Query constructor.
		 *
		 * @param array $args
		 */
		function __construct( $args = array() ) {

		$args = wp_parse_args( $args, array(
			'post_type' => AFAC_CM_Campaign::CPT,
			'orderby' => 'title',
			'order' => 'ASC',
			// Turn off paging
			'posts_per_page' => -1,
			// Since, we won't be paging,
			// no need to count rows
			'no_found_rows' => true,
			'post_status'	=> array('publish')
		) );

		parent::__construct( $args );

	}
}

