<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly.

/**
* Class AFAC_CM_Notification
*/
class AFAC_CM_Notification {

	/**
	 * @var string
	 */
	private $to_email = '';

	/**
	 * @var string
	 */
	private $to_name = '';

	/**
	 * @var string
	 */
	private $subject = '';

	/**
	 * @var array
	 */
	private $headers;

	/**
	 * @var string
	 */
	private $body = '';

	/**
	 * @var string
	 */
	private $from_email = '';

	/**
	 * @var string
	 */
	private $from_name = '';

	/**
	 * @var array
	 */
	private $placeholders;

	/**
	 * @var bool
	 */
	public $valid_placeholder = false;

	/**
	 * AFAC_CM_Notification constructor.
	 *
	 * @param null $template_id
	 */
	public function __construct( $template_id = null ) {

		$this->placeholders = array();
		$this->headers = array('Content-Type: text/html; charset=UTF-8');

		if ( ! empty( $template_id ) ) {
			$this->set_template( $template_id );
		}

	}

		/**
		 * @param $template_id
		 */
		public function set_template( $template_id ) {

			$this->template = $template_id;

			$option = afac_cm_get_option();

			foreach ( array( 'subject', 'from_name', 'from_email', 'body' ) as $field ) {

				if ( isset( $option[ $template_id . '_' . $field ] ) ) {
					$this->{$field} = $option[ $template_id . '_' . $field ];
				} else {
					$this->valid_placeholder = false;

					return;
				}
			}

			$this->valid_placeholder = true;

		}

		/**
		 * @param array $args
		 */
		public function set_placeholders( $args = array() ) {

			foreach ( $args as $placeholder => $value ) {

				$this->placeholders[ sprintf( '{%s}', $placeholder ) ] = $value;

			}
		}

		/**
		 * @param $email
		 * @param string $name
		 */
		public function to( $email, $name = '' ){

			if( ! empty( $email ) ) $this->to_email = $email;
			$this->to_name = $name;
		}

		/**
		 * @return bool
		 */
		public function send(){

			if( empty( $this->to_email ) ) return false;

			if( empty( $this->to_name ) ){
				$this->send_to = $this->to_email;
			} else {
				$this->send_to = sprintf( '%s <%s>', $this->to_name, $this->to_email );
			}

			$this->parse_template();

			$from_name = wp_specialchars_decode( $this->from_name, ENT_QUOTES );
			$from_name = apply_filters( 'afac_campaign_notice_from_name', $from_name, $this );

			$from_email = $this->from_email;
			$from_email = apply_filters( 'afac_campaign_notice_from_address', $from_email, $this );

			$subject = apply_filters( 'afac_campaign_notice_subject', wp_strip_all_tags( $this->parsed_subject ), $this );

			$emails = Give()->emails;
			$emails->__set( 'from_name', $from_name );
			$emails->__set( 'from_email', $from_email );
			$emails->__set( 'heading', $this->parsed_subject );


			$headers = apply_filters( 'afac_campaign_notice_headers', $emails->get_headers(), $this );
			$emails->__set( 'headers', $headers );

			$emails->send( $this->send_to, $subject, $this->parsed_body );

	}

		/**
		 *
		 */
		private function parse_template(){

		$this->parse_subject();

		$this->parse_body();

	}

		/**
		 *
		 */
		private function parse_subject(){
		$this->parsed_subject = str_replace( array_keys( $this->placeholders ), $this->placeholders, $this->subject );
	}

		/**
		 *
		 */
		private function parse_body(){
		$this->parsed_body = str_replace( array_keys( $this->placeholders ), $this->placeholders, wpautop($this->body) );
	}

	public static function add_email_tags( $email_tags ){

		$email_tags[] = array(
				'tag'         => 'campaign',
				'desc' => __( 'Name of the Personal Campaign (if applicable).', 'afac_cm' ),
				'func'    => 'afac_cm_campaign_title',
				'context'	    => 'donation'
		);

		return $email_tags;

	}
	
	public static function pdf_receipt( $template_content, $args ){
	
		$tag_args = array( 'payment_id' => $args['donation_id'] );
		
		$campaign_name = afac_cm_campaign_title( $tag_args );
		
		$tmpl = str_replace( '{campaign}', $campaign_name, $template_content );

		return $tmpl;

	}

}

	/**
	 * Email template tag: campaign
	 * The title of the campaign
	 *
	 * @param int $payment_id
	 *
	 * @return string campaign title
	 */
	function afac_cm_campaign_title( $tag_args ){

		$payment_data = give_get_payment_meta( $tag_args['payment_id'] );
		if ( !isset( $payment_data['_afac_campaign_id'] ) ) {
			return '';
		}

		$campaign = new AFAC_Campaign( $payment_data['_afac_campaign_id'] );

		return $campaign->post_title;

}
