<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class AFAC_CM_Notifications {

	public static function get_placeholders(){

		$placeholders = array(
				'campaign'	=> "Campaign Title",
				'organizer'	=> "Campaign Organizer Name",
				'donation'	=> "Amount of the donation",
				'goal'		=> "Campaign Goal",
				'raised'	=> "Total Raised-to-date",
				'link'		=> "Url to Campaign page",
		);

		return $placeholders;
	}


	public static function donation_received( $to, $subject, $values ){}

	public static function goal_met(){}

	public static function campaign_approved(){}

	public static function campaign_is_ending(){}

	private static function replace_placeholders(){}

}