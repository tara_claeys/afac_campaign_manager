<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class AFAC_CM_Settings {

	private static $_this;

	/**
 	 * Option key, and option page slug
 	 * @var string
 	 */
	public static $key = 'afac_cm_options';

	/**
	 * Public getter method for retrieving protected/private variables
	 * @since  0.1.0
	 * @param  string  $field Field to retrieve
	 * @return mixed          Field value or exception is thrown
	 */
	public function __get( $field ) {
		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}

		throw new Exception( 'Invalid property: ' . $field );
	}

}
